var _PM, // page manager
    _FB, // the FB plugin
    _SM; // the settings manager

/**
 * @author nica hy.p.nicolacastellani@gmail.com
 * @since 20161117
 * @description google maps plugin callback after load
 * 
 * @private
 */

InitMap = function(){
    console.log('[MAIN] GMap loaded');
}

/**
 * @author nica hy.p.nicolacastellani@gmail.com
 * @since 20161117
 * @description this is the main function, don't manage here pages behaviours but use custom js in sitestructure.json
 * 
 * @private
*/

$(document).ready(function(){
    //_FB = new _FB('169711523493184', '169711523493184|ZJcEc66z4gtwtBT-rwGYY16I-VA');
    new SettingsManager(function(_SM){
        this._SM = _SM;
        _PM = new PageManager(_SM);
        _SM.getDefaultPage(function(page){
            _PM.GoTo(page); // load homepage
        });
    });
});
