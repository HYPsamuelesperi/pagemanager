var Page1JS = function(){
    this.init();
}

/**
 * @author nica
 * @since 20161117
 * @description custom js for home
 */

Page1JS.prototype = {
    init: function() {
        console.log("Page js loaded");
    },

    fnOnEnter: function (fnAfterEnter){
        console.log('[PAGEJS] >>> on load');
        (fnAfterEnter || $.noop)();
    },

    fnOnExit: function (fnAfterExit){
        console.log('[PAGEJS] >>> on exit');
        (fnAfterExit || $.noop)();
    }
}