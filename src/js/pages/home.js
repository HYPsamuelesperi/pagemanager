var HomeJS = function(){
    this.init();
}

/**
 * @author nica
 * @since 20161117
 * @description custom js for home
 */

HomeJS.prototype = {
    init: function() {
        console.log("Home js loaded");
    },

    fnOnEnter: function (fnAfterEnter){
        console.log('[HOMEJS] >>> on load');
        _PM.$container.find('.link').on('click.hlink',function(){
            _SM.getPageByName($(this).data('page'), function(targetPage){
                _PM.GoTo(targetPage);
            });
        });

        (fnAfterEnter || $.noop)();
    },

    fnOnExit: function (fnAfterExit){
        console.log('[HOMEJS] >>> on exit');
        (fnAfterExit || $.noop)();
    }
}