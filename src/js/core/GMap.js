var GMap = function($container, options){
    this.init($container, options);
}

GMap.prototype = {

    _map: null, // gmap object
    _mapOptions: null, // gmap object options

    init: function($container, options) {
        var self = this;
        
        self._mapOptions = {zoom: 4}; // default options

        $.extend(self._mapOptions, options); // override with custom options

        self._map = new google.maps.Map($container[0], self._mapOptions);

        google.maps.event.trigger(self._map, "resize");

        $(window).off('resize.map').on('resize.map',function(){
            google.maps.event.trigger(self._map, "resize");
        });

        console.log('[GMAP] >>> map created on container');
    },
    /** */
    CreateMarker: function(pos, icon) {
        var self = this,
            marker = new google.maps.Marker({
                position: pos,
                map: self._map,
                icon: icon
            });

            self._map.setCenter(pos);
    },

    Resize: function(timeout, posToCenter){
        var self = this;
        setTimeout(function(){
            google.maps.event.trigger(self._map, "resize")
            if(posToCenter) {
                self._map.setCenter(posToCenter);
            }
        }, timeout || 0);
    },
}