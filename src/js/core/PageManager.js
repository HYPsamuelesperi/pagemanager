var PageManager = function(_SM){
    this.init(_SM);
}
/**
 * @author nica
 * @since 20161117
 * @description Pagemanager manage page changes and links behaviours
 */

PageManager.prototype = {

    _fadeToDelay: 250, // default delay bedfore showing content in fadeto function
    $container: null,
    $loader:null,
    $menu: null,
    $menutrigger: null,
    activepage: null,
    _onImageModal: false,
    _onMapSelected: false,
    _SM: null, // settings manager reference

    /**
     * @author nica
     * @since 20161117
     * @description init pagemanager standard attributes and behaviours
     */
    
    init: function(_SM) {
        var self = this;
        console.log('[PM] >>> init page manager');

        self.$menu = $('#menu'); // store menu var
        self.$container = $('#container'); // store main container
        self.$loader = $('#loader'); // store main loader
        self.$menutrigger = $('.menu-link');
        self._SM = _SM;

        _SM.getMenuSettings(function(mSettings) {
            self.BuildMenu(mSettings);
        });

        self.ManageMenuTrigger();
    },

    /* UTILS GOES HERE */

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description fade from an element to another
     * @param {JSON} options custom options for the fading process, use {delay: 250, fadeOutClass: 'xxx', fadeInClass: 'xxx'}
     */

    FadeTo: function($from, $to, callback, options) {
        var self = this;
        options = options || {};
        $from.addClass(options.fadeOutClass || 'fade-out-slow'); // fade out from element
        setTimeout(function(){
            $to.removeClass('hidden fade-out-slow').addClass(options.fadeInClass || 'fade-in-slow'); // remove hidden classes and add fade-in-slow
            $from.addClass('hidden');
            (callback || $.noop)();
        }, options.delay || this.standardDelay);
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description manage main menu behaviour
     * 
     * @private
     */

    InitMenu: function(){
        var self = this;
        self.$menu.find('.menu-item').off('click.mitem').on('click.mitem',function(){
            var $this = $(this);
            _SM.getPageByName($this.data('page'), function(page) {
                if(page != self.activepage) {
                    self.GoTo(page);
                }
                self.HideMenu();
            });
        });
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description Goto another page
     * 
     * @private
     */

    GoTo: function(page) {
        var self = this,
            fnShowCurrent = function(){
                self.FadeTo(self.$loader, self.$container);
                self.$menutrigger.toggle(page.showMenu);
            },
            fnHideCurrent = function(){
                self.FadeTo(self.$container, self.$loader, function(){
                    self.$container.load('/src/templates/pages/' + page.template, function(){ // load home template
                        if(page.customjs && page.customjs[page.fnOnEnter] && page.fnOnEnter) {
                            console.log('[PM] >>> calling custom js on enter');
                            page.customjs[page.fnOnEnter](fnShowCurrent);
                        } else {
                            fnShowCurrent();
                        }
                    });
                });
            };

        console.log('[PM] >>> going to page ' + page.name);

        //reset def
        self._onMapSelected = false;
        self._onImageModal = false;

        if(self.activepage && self.activepage.customjs && page.customjs[page.fnOnExit] && self.activepage.fnOnExit) {
            console.log('[PM] >>> calling custom js on exit');
            page.customjs[page.fnOnExit](fnHideCurrent);
        } else {
            fnHideCurrent();
        }

        self.activepage = page.name; // update current page
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description show main menu
     * 
     * @private
     */

    ShowMenu: function(){
        var self = this;
        console.log('[PM] >>> show menu for active page');
        self.$menu.removeClass('hidden fade-out-fast').addClass('fade-in-fast');
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description hide main menu
     * 
     * @private
     */

    HideMenu: function() {
        var self = this;
        console.log('[PM] >>> hide menu for active page');
        self.$menu.removeClass('fade-in-fast').addClass('fade-out-fast hidden');
        self.$menutrigger.removeClass('close');
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description show next gallery image
     * 
     * @private
     */

    fnGoToNextImage: function(){
        var self = this,
            $currentImg = self.$container.find('.gallery-cell[selected]');
        $currentImg.removeAttr('selected');

        if($currentImg.next().length == 0) {
            self.$container.find('.image-modal').click();
        } else {
            $currentImg.next().click();
        }
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description show previous image
     * 
     * @private
     */

    fnGoToPreviousImage: function(){
        var self = this,
            $currentImg = self.$container.find('.gallery-cell[selected]');
        $currentImg.removeAttr('selected');

        if($currentImg.prev().length == 0) {
            self.$container.find('.image-modal').click();
        } else {
            $currentImg.prev().click();
        }
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description manage hammer js plugin
     * 
     * @private
     */

    ManageHammer: function(){
        var self = this,
            hammertime = new Hammer(document.body, { treshold: 1500, velocity:1 });

        hammertime.on('swiperight', function(ev) {
            if(self.activepage.name !== 'home' && !self._onImageModal && !self._onMapSelected) {
                self.GoTo('home');
            } else if(self._onImageModal) {
                self.fnGoToPreviousImage();
            }
        });

        hammertime.on('swipeleft', function(ev) {
            if(self._onImageModal) {
                self.fnGoToNextImage();
            }
        });
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161117
     * @description manage menu-trigger behaviours
     * 
     * @private
     */

    ManageMenuTrigger: function(){
        var self = this;
        self.$menutrigger.off('click.mtrigger').on('click.mtrigger', function(ev) {
            $(this).toggleClass('close');
            if(self.$menu.hasClass('hidden')){
                self.ShowMenu();
            } else {
                self.HideMenu();
            }
        });
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description create menu using menusettings
     * 
     * @private
     */

    BuildMenu: function(mSettings){
        console.log('[PM] >>> building menu');
        var self = this,
            menuInnerHtml = '';

        for(var index in mSettings) {
            menuInnerHtml += '<div class="menu-item" data-page="' + mSettings[index].goTo + '">' + mSettings[index].label + '</div>'
        }

        self.$menu.append(menuInnerHtml);

        self.InitMenu();
    }

    /* END OF UTILS */
}