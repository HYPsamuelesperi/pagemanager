var SettingsManager = function(fnGoToDefaultPage){
    this.init(fnGoToDefaultPage);
}

/**
 * @author nica
 * @since 20161117
 * @description SettingsManager managejson settings files
 */

SettingsManager.prototype = {
    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description init settings manager
     * 
     */
    init: function(fnGoToDefaultPage) {
        console.log("Settings manager loaded");

        this.initSettings(fnGoToDefaultPage);
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description add custom js and custom css according with sitestructure.json
     * 
     * @private
     */

    initSettings: function(fnGoToDefaultPage){
        var self = this;
        for(var index in __sstructure) {
            if(__sstructure[index].customjs) {
                self.addScript('/src/js/pages/' + __sstructure[index].customjs, __sstructure[index]);
            }
            if(__sstructure[index].customcss) {
                self.addCSSFile('/src/css/pages/' + __sstructure[index].customcss);
            }
        }
        (fnGoToDefaultPage || $.noop)(self);
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description add custom script to head
     * 
     * @private
     */

    addScript: function(path, page) {
        var s = document.createElement('script');
        s.src = path;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
            if ((!s.readyState || /loaded|complete/.test(s.readyState))) {
                console.log('[SM] >>> script at path ' + path + ' loaded..');
                page.customjs = page.oninit();
            }
        };
        document.querySelector('head').appendChild(s);
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description add custom stylesheet to head
     * 
     * @private
     */

    addCSSFile: function(path, callback) {
        var head = document.getElementsByTagName("head")[0];
        var s = document.createElement("link");
        s.rel = "stylesheet";
        s.href = path;
        head.appendChild(s);
        console.log('[SM] >>> css at path ' + path + ' loaded..');
        (callback || $.noop)();
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description get default page object according with sitestructure.json
     * 
     * @private
     */

    getDefaultPage: function(callback){
        for(var index in __sstructure) {
            if(__sstructure[index].isdefault) {
                callback(__sstructure[index]);
                return;
            }
        }
    },
    
    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description get page object by name, according with sitestructure.json
     * 
     * @private
     */

    getPageByName: function(pagename, callback){
        for(var index in __sstructure) {
            if(__sstructure[index].name == pagename) {
                callback(__sstructure[index]);
                return;
            }
        }
    },

    /**
     * @author nica hy.p.nicolacastellani@gmail.com
     * @since 20161201
     * @description get menu setting object, according with menusettings.json
     * 
     * @private
     */

    getMenuSettings: function(callback) {
        callback(__mstructure || {});
    }
}