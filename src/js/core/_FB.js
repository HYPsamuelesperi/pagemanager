var _FB = function(appId, access_token){
    this.init(appId, access_token);
}

_FB.prototype = {
    _appId: null,
    _accToken: null,
    init: function(appId, access_token) {
        var self = this;
        self._appId = appId;
        self._accToken = access_token;
        window.fbAsyncInit = function() {
            FB.init({
                appId      : self._appId,
                xfbml      : true,
                version    : 'v2.8'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    },

    GetAlbumPhotos(albumId, callback) {
        var self = this,
            photos = [],
            photoslength = 0,
            count = 0;

        FB.api(
            "/" + albumId + "/photos",
            {
                access_token: self._accToken
            },
            function (response) {
                if (response && !response.error) {
                    photoslength = response.data.length;
                    for(var i = 0; i < photoslength; i++) {
                        FB.api(
                            '/' + response.data[i].id + '/picture',
                            {
                                access_token: self._accToken
                            },
                            function (photo) {
                                if (photo && !photo.error) {
                                    $.extend(response.data[count], photo.data);
                                    photos.push(response.data[count]);
                                    count++;
                                    if(photos.length == photoslength){
                                        (callback || $.noop)(photos);
                                    }
                                }
                            });
                    }
                }
            }
        );
    }
}